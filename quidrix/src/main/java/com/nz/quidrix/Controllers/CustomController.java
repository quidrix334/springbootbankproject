package com.nz.quidrix.Controllers;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("api/custom")
public class CustomController {

    @GetMapping
    public String Custom(){
        return "Hello From Custom Controller";
    }
    @GetMapping("hello")
    public String Hello(){
        return "hi";
    }
}
