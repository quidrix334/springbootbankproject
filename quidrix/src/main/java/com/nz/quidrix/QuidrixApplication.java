package com.nz.quidrix;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class QuidrixApplication {

	public static void main(String[] args) {
		SpringApplication.run(QuidrixApplication.class, args);
	}

}
